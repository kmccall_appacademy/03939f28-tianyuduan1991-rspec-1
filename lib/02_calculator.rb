def add(v1, v2)
 v1 + v2
end

def subtract(v1, v2)
  v1 - v2
end

def sum(arr)
   sum = 0
   arr.each { |number| sum += number }
   sum
end

def multiply(arr)
  arr.reduce(:*)
end

def power(base, exponent)
  base ** exponent
end

def factorial(n)
fact_sum = 1
  n.downto(1).each do |i|
    fact_sum  *= i
  end
fact_sum
end
